import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {BackRoutingModule} from './back.routing.module';
import {BackProfileComponent} from './pages/profile/profile.component';
import {BackMainComponent} from './back.component';
import {BackHistoryComponent} from './pages/history/history.component';
import {BackSettingsComponent} from './pages/settings/settings.component';
import {TeethComponent} from './components/teeth/teeth.component';
import {CalendarComponent} from './pages/calendar/calendar.component';

@NgModule({
  imports: [
    SharedModule,
    BackRoutingModule
  ],
  declarations: [
    BackProfileComponent,
    BackMainComponent,
    BackHistoryComponent,
    BackSettingsComponent,
    TeethComponent,
    CalendarComponent
  ]
})
export class BackModule {
}
