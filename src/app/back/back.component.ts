import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-back',
  templateUrl: './back.component.html',
  styleUrls: ['./back.component.scss']
})
export class BackMainComponent implements OnInit {
  menu = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }


  onDeactivate() {
    document.body.scrollTop = 0;
  }

  logOut() {
    this.router.navigate(['']);
  }
}
