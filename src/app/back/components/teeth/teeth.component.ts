import {Component, EventEmitter, Input, Output} from '@angular/core';


@Component({
  selector: 'app-teeth',
  templateUrl: 'teeth.component.html',
  styleUrls: ['./teeth.component.scss']
})
export class TeethComponent {
  @Input() teeth: any;
  @Output() onTooth = new EventEmitter();
  
  constructor() {
  }
  
  tooth(i) {
    this.onTooth.emit(i);
  }
}
