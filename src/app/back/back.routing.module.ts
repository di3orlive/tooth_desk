import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BackMainComponent} from './back.component';
import {BackProfileComponent} from './pages/profile/profile.component';
import {BackHistoryComponent} from './pages/history/history.component';
import {BackSettingsComponent} from './pages/settings/settings.component';
import {CalendarComponent} from './pages/calendar/calendar.component';

const routes: Routes = [
  {
    path: '',
    component: BackMainComponent,
    children: [
      {path: 'profile', component: BackProfileComponent},
      {path: 'history', component: BackHistoryComponent},
      {path: 'calendar', component: CalendarComponent},
      {path: 'settings', component: BackSettingsComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackRoutingModule {
}



