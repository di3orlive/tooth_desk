import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackHistoryComponent } from './history.component';

describe('BackHistoryComponent', () => {
  let component: BackHistoryComponent;
  let fixture: ComponentFixture<BackHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
