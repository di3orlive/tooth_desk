import {Component, HostBinding, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {Router} from '@angular/router';
import {Animations} from '../../../front/animations/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: Animations.page
})
export class LoginComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  type = 'login';
  loginForm: FormGroup;
  registerForm: FormGroup;
  forgotForm: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder
  ) {
    this.forgotForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });



    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });



    const password = this.fb.control('', Validators.required);
    const password2 = this.fb.control('', [Validators.required, CustomValidators.equalTo(password)]);
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: password,
      password2: password2
    });
  }

  ngOnInit() {
  }


  login() {
    this.router.navigate(['dashboard/profile']);
  }


  register() {

  }
}
