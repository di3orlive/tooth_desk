import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';


@Injectable()
export class HttpService {
  apiUrl = 'https://di3calendar.herokuapp.com';

  constructor(
    private router: Router,
    private http: HttpClient,
  ) {
  }

  get(url: string, params: any = {}): Observable<HttpResponse<Object>> {
    return this.http.get<Object>(this.apiUrl + url, this.addOptions(this.toHttpParams(params))).catch(error => this.handleError(error));
  }

  post(url: string, body: any = {}): Observable<HttpResponse<Object>> {
    return this.http.post<Object>(this.apiUrl + url, body, this.addOptions()).catch(error => this.handleError(error));
  }

  put(url: string, body: any = {}): Observable<HttpResponse<Object>> {
    return this.http.put<Object>(this.apiUrl + url, body, this.addOptions()).catch(error => this.handleError(error));
  }

  delete(url: string): Observable<HttpResponse<Object>> {
    return this.http.delete<Object>(this.apiUrl + url, this.addOptions()).catch(error => this.handleError(error));
  }

  private toHttpParams(params) {
    return Object.getOwnPropertyNames(params)
      .reduce((p, key) => p.set(key, params[key]), new HttpParams());
  }

  private addOptions(params?: HttpParams) {
    const options = {};
    if (params) {
      options['params'] = params;
    }

    const token = localStorage.getItem('scUserToken');
    if (token) {
      options['headers'] = new HttpHeaders({
        'Authorization': token
      });
    }

    options['observe'] = 'response';
    return options;
  }

  private handleError(error: any) {
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401) {
        localStorage.removeItem('scUserToken');
        this.router.navigate(['/signin']);
      }

      return Observable.throw(error);
    }
  }
}
