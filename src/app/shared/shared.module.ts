import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {MaterialModule} from './material.module';
import {Animations} from '../front/animations/animations';
import {
CalendarDateFormatter, CalendarModule, CalendarNativeDateFormatter,
DateFormatterParams
} from 'angular-calendar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CustomFormsModule} from 'ng2-validation';
import {InlineSVGModule} from 'ng-inline-svg';
import {ToastyModule} from 'ng2-toasty';
import {NgxCarouselModule} from 'ngx-carousel';
import {FileUploadModule} from 'ng2-file-upload';
import {HttpClientModule} from '@angular/common/http';
import {HttpService} from '../common/services/http/http.service';
import {HttpModule} from '@angular/http';


export class CustomDateFormatter extends CalendarNativeDateFormatter {
  public dayViewHour({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat('ca', {
      hour: 'numeric',
      minute: 'numeric'
    }).format(date);
  }
}


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    MaterialModule,
    InlineSVGModule,
    NgxCarouselModule,
    CalendarModule.forRoot({
      dateFormatter: {
        provide: CalendarDateFormatter,
        useClass: CustomDateFormatter
      }
    }),
    ToastyModule.forRoot(),
    FileUploadModule
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    MaterialModule,
    InlineSVGModule,
    NgxCarouselModule,
    CalendarModule,
    ToastyModule,
    FileUploadModule
  ],
  providers: [
    Animations,
    DatePipe,
    HttpService
  ]
})
export class SharedModule {
}
