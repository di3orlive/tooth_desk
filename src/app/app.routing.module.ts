import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './common/pages/login/login.component';
import {PageNotFoundComponent} from './common/pages/page-not-found/page-not-found.component';


const appRoutes: Routes = [
  {path: '', loadChildren: 'app/front/front.module#FrontModule'},
  {path: 'dashboard', loadChildren: 'app/back/back.module#BackModule'},
  {path: 'login', component: LoginComponent},
  {path: '**', component: PageNotFoundComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class RoutingModule {
}

