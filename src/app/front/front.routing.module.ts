import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FrontMainComponent} from './front.component';
import {HomeComponent} from './pages/home/home.component';
import {ServicesComponent} from './pages/services/services.component';
import {ServicesArticleComponent} from './pages/services-article/services-article.component';
import {TeamPageComponent} from './pages/team/team.component';
import {TeamArticleComponent} from './pages/team-article/team-article.component';
import {DiagnosisComponent} from './pages/diagnosis/diagnosis.component';
import {GalleryComponent} from './pages/gallery/gallery.component';
import {GalleryArticleComponent} from './pages/gallery-article/gallery-article.component';
import {EducationalComponent} from './pages/educational/educational.component';
import {How2careComponent} from './pages/how2care/how2care.component';
import {ContactsComponent} from './pages/contacts/contacts.component';
import {AboutComponent} from './pages/about/about.component';
import {OurClinicComponent} from './pages/our-clinic/our-clinic.component';
import {AwardsComponent} from './pages/awards/awards.component';

const routes: Routes = [
  {
    path: '',
    component: FrontMainComponent,
    children: [
      {path: '', redirectTo: 'home'},
      {path: 'home', component: HomeComponent},
      {path: 'about', component: AboutComponent},
      {path: 'services', component: ServicesComponent},
      {path: 'service/:serviceId', component: ServicesArticleComponent},
      {path: 'team', component: TeamPageComponent},
      {path: 'team/:docId', component: TeamArticleComponent},
      {path: 'diagnosis', component: DiagnosisComponent},
      {path: 'gallery', component: GalleryComponent},
      {path: 'gallery/:name', component: GalleryArticleComponent},
      {path: 'educational', component: EducationalComponent},
      {path: 'how2care', component: How2careComponent},
      {path: 'our-clinic', component: OurClinicComponent},
      {path: 'awards', component: AwardsComponent},
      {path: 'contacts', component: ContactsComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontRoutingModule {
}
