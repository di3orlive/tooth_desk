import {NgModule} from '@angular/core';
// external modules
import {NgxGalleryModule} from 'ngx-gallery';
// pipes
import {SafePipe} from './pipes/safe/safe.pipe';
// shared main
import {SharedModule} from '../shared/shared.module';
import {FrontRoutingModule} from './front.routing.module';
import {FrontMainComponent} from './front.component';
// services
import {CalendarService} from './services/calendar/calendar.service';
import {SideNavService} from './services/side-nav/side-nav.service';
import {ServicesService} from './services/services/services.service';
import {DoctorsService} from './services/doctors/doctors.service';
// pops
import {AppointmentPopComponent} from './pops/appointment/appointment.component';
import {AppointmentAddPopComponent} from './pops/appointment-add/appointment-add.component';
import {CallMePopComponent} from './pops/call-me/call-me.component';
import {MapPopComponent} from './pops/map/map.component';
import {CareersComponent} from './pops/careers/careers.component';
import {FeedbackComponent} from './pops/feedback/feedback.component';
// components
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {MapComponent} from './components/map/map.component';
import {MenuComponent} from './components/menu/menu.component';
import {TeamComponent} from './components/team/team.component';
import {LogoComponent} from './components/logo/logo.component';
// pages
import {HomeComponent} from './pages/home/home.component';
import {ServicesComponent} from './pages/services/services.component';
import {TeamPageComponent} from './pages/team/team.component';
import {DiagnosisComponent} from './pages/diagnosis/diagnosis.component';
import {GalleryComponent} from './pages/gallery/gallery.component';
import {EducationalComponent} from './pages/educational/educational.component';
import {How2careComponent} from './pages/how2care/how2care.component';
import {ContactsComponent} from './pages/contacts/contacts.component';
import {GalleryArticleComponent} from './pages/gallery-article/gallery-article.component';
import {TeamArticleComponent} from './pages/team-article/team-article.component';
import {ServicesArticleComponent} from './pages/services-article/services-article.component';
import {AboutComponent} from './pages/about/about.component';
import {OurClinicComponent} from './pages/our-clinic/our-clinic.component';
import {AwardsComponent} from './pages/awards/awards.component';



@NgModule({
  imports: [
    SharedModule,
    FrontRoutingModule,
    NgxGalleryModule
  ],
  declarations: [
    HomeComponent,
    LogoComponent,
    ServicesComponent,
    TeamPageComponent,
    DiagnosisComponent,
    GalleryComponent,
    EducationalComponent,
    How2careComponent,
    ContactsComponent,
    HeaderComponent,
    FooterComponent,
    MapComponent,
    GalleryArticleComponent,
    TeamArticleComponent,
    ServicesArticleComponent,
    FrontMainComponent,
    MenuComponent,
    TeamComponent,
    AboutComponent,
    OurClinicComponent,
    AwardsComponent,
    AppointmentPopComponent,
    AppointmentAddPopComponent,
    CallMePopComponent,
    MapPopComponent,
    CareersComponent,
    FeedbackComponent,
    SafePipe
  ],
  entryComponents: [
    AppointmentPopComponent,
    AppointmentAddPopComponent,
    CallMePopComponent,
    CareersComponent,
    MapPopComponent,
    FeedbackComponent
  ],
  providers: [
    CalendarService,
    SideNavService,
    ServicesService,
    DoctorsService
  ]
})
export class FrontModule {
}
