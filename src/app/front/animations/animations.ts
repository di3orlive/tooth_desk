import {style, animate, transition, state, trigger} from '@angular/animations';

export class Animations {
  static page = [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', [
        style({opacity: 0}), animate('300ms linear')
      ]),
      transition('* => void',
        style({display: 'none'})
      )
    ])
  ];
}
