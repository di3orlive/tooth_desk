import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CareersComponent} from '../../pops/careers/careers.component';
import {MatDialog} from '@angular/material';
import {SideNavService} from '../../services/side-nav/side-nav.service';
import {FeedbackComponent} from '../../pops/feedback/feedback.component';

@Component({
  selector: 'app-front-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  links = [
    {
      title: 'Home',
      url: '/',
    },
    {
      title: 'Abouts Us',
      url: '/about',
    },
    {
      title: 'Services',
      url: '/services',
    },
    {
      title: 'Team',
      url: '/team',
    },
    {
      title: 'Gallery of works',
      url: '/gallery',
    },
    {
      title: 'Educational video',
      url: '/educational',
    },
    {
      title: 'How to care for your teeth',
      url: '/how2care',
    },
    {
      title: 'Our clinic',
      url: '/our-clinic',
    },
    {
      title: 'Awards',
      url: '/awards',
    },
    {
      title: 'Careers',
      url: '',
      pop: CareersComponent
    },
    {
      title: 'Feedback',
      url: '',
      pop: FeedbackComponent
    },
    {
      title: 'Contacts',
      url: '/contacts',
    }
  ];

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private sideNavService: SideNavService
  ) { }

  ngOnInit() {
  }

  goTo(item) {
    if (!item.url) {
      this.dialog.open(item.pop, {
        width: '500px'
      });
      this.sideNavService.toggle();
    } else {
      this.router.navigate([item.url]);
    }
  }
}


