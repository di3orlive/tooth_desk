import {Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() map: any;

  constructor() {
  }

  ngOnInit () {
    // console.log(document.getElementById('map')); // wtf


    const x = setInterval(() => {
      if (document.getElementById('map')) {
        clearInterval(x);
        this.map();
      }
    }, 100);
  }
}

