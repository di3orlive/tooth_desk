import {Component, OnInit} from '@angular/core';
import {SafeSubscribe} from '../../../common/helpers/safe-subscripe/safe-subscripe';
import {AppointmentPopComponent} from '../../pops/appointment/appointment.component';
import {MatDialog} from '@angular/material';
import {CallMePopComponent} from '../../pops/call-me/call-me.component';
import {SideNavService} from '../../services/side-nav/side-nav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends SafeSubscribe implements OnInit {
  sideNav: any;

  constructor(
    public dialog: MatDialog,
    private sideNavService: SideNavService
  ) {
    super();
  }

  ngOnInit() {
    this.sideNav = this.sideNavService.sideNav;
  }

  appointmentPop() {
    this.dialog.open(AppointmentPopComponent);
  }

  callMePop() {
    this.dialog.open(CallMePopComponent);
  }

  toggleSideNav() {
    this.sideNavService.toggle().then(() => { });
  }
}
