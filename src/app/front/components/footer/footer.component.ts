import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {MapPopComponent} from '../../pops/map/map.component';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(
    public dialog: MatDialog
  ) { }


  ngOnInit() {
  }


  openMap() {
    this.dialog.open(MapPopComponent);
  }
}
