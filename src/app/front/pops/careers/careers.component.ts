import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {FileUploader} from 'ng2-file-upload';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.scss']
})
export class CareersComponent {
  form: FormGroup;
  uploader: FileUploader = new FileUploader({});
  jobTitle = [
    {value: 'General Dentist'},
    {value: 'Specialized Dentist'},
    {value: 'Reception'},
    {value: 'Dental Assistant'},
    {value: 'Others'}
  ];

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
  ) {
    this.createForm();

    this.uploader.onAfterAddingFile = (fileItem: any) => {
      this.form.get('file').setValue(fileItem);
      this.form.get('file').clearValidators();
      this.form.get('file').updateValueAndValidity();
    };
  }


  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      phone: ['', Validators.pattern(/^[\d ()+-]+$/)],
      email: ['', Validators.email],
      jobTitle: ['', Validators.required],
      file: ['', Validators.required]
    });
  }

  addFile(fileInput) {
    fileInput.click();
  }

  clearFiles(fileInput) {
    fileInput.value = '';
    this.form.get('file').reset();
    this.form.get('file').setValidators(Validators.required);
    this.form.get('file').updateValueAndValidity();
  }

  submitForm() {
    const url = 'https://hooks.slack.com/services/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';
    const body = {
      channel: '#general',
      username: 'CAREERS REQUEST',
      icon_emoji: ':monkey_face:',
      text: 'yo',
      // ...this.form.value
    };

    console.log(body);

    this.http.post(url, body).subscribe(res => {
      console.log(res);
    });
  }
}
