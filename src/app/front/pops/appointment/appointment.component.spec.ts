import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentPopComponent } from './appointment.component';

describe('AppointmentPopComponent', () => {
  let component: AppointmentPopComponent;
  let fixture: ComponentFixture<AppointmentPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentPopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
