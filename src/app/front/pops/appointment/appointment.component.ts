import {ChangeDetectionStrategy, Component} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {AppointmentAddPopComponent} from '../appointment-add/appointment-add.component';
import {DatePipe} from '@angular/common';
import {SafeSubscribe} from '../../../common/helpers/safe-subscripe/safe-subscripe';
import {CalendarService} from '../../services/calendar/calendar.service';
import {ToastyService} from 'ng2-toasty';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {DoctorsService} from '../../services/doctors/doctors.service';


@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./appointment.component.scss']
})
export class AppointmentPopComponent extends SafeSubscribe {
  step = 0;
  doctors: any;
  curDoc: any;


  curDayId: any;
  hrInterval: any;
  week = [];
  viewDate: Date = new Date();
  colors: any = {
    past: {
      primary: '#E64A19',
      secondary: '#E64A19'
    },
    future: {
      primary: '#4caf50',
      secondary: '#4caf50'
    },
    present: {
      primary: '#448AFF',
      secondary: '#448AFF'
    }
  };
  events = new BehaviorSubject<any>([]);
  doctor: any;


  constructor(
    private calendarService: CalendarService,
    private datePipe: DatePipe,
    private toastyService: ToastyService,
    private doctorsService: DoctorsService,
    public dialog: MatDialog
  ) {
    super();
    this.dateInit();


    this.doctorsService.doctors.safeSubscribe(this, res => {
      this.doctors = res;
    });
  }



  get eventsAsync() {
    return this.events.asObservable();
  }

  getCalendarEvents(params) {
    // let loader = this.loadingCtrl.create({
    //   spinner: 'hide',
    //   content: '<img src='./assets/icon/loading.gif' style='height: 100px'>',
    // });
    // loader.present();

    this.calendarService.getDayEvents(params, '6orus9@gmail.com').safeSubscribe(this, (res: any) => {
      this.events.next([]);
      const events = [];

      res.forEach((item) => {
        const now = +new Date();
        const start = new Date(item.start.dateTime);
        const end = new Date(item.end.dateTime);
        let color: any;

        if (now >= +start && now <= +end) {
          color = this.colors.present;
        }
        if (now >= +end) {
          color = this.colors.past;
        }
        if (now <= +start) {
          color = this.colors.future;
        }

        events.push({
          start: start,
          end: end,
          title: item.summary,
          color: color
        });
      });

      this.events.next(events);
    });
  }


  addEvent(date, e) {
    console.log(e);

    // const now = +new Date();
    // const end = new Date(date);
    // if (now >= +end) {
    //   this.toastyService.default({
    //     title: 'You can not book this time',
    //     showClose: true,
    //   });
    //
    //   // let toast = this.toastCtrl.create({
    //   //   message: 'You can not book this time',
    //   //   duration: 2000,
    //   //   showCloseButton: true,
    //   //   closeButtonText: 'Ok'
    //   // });
    //   // toast.present();
    //   return;
    // }


    const dialogRef = this.dialog.open(AppointmentAddPopComponent, <MatDialogConfig>{data: date, doctor: this.doctor});

    dialogRef.afterClosed().subscribe(res => {
      if (!!res) {
        console.log(res);
        this.toastyService.success({
          title: 'One of our team will contact you within the next 24hrs',
          showClose: true,
          timeout: 3000
        });

        this.calendarService.insertEvent(res, '6orus9@gmail.com').safeSubscribe(this, () => {
          this.getCalendarEvents(this.week[this.curDayId].params);
        });
      }
    });
  }


  dateInit() {
    for (let i = 0; i < 7; i++) {
      const dateMin = new Date(new Date().setDate(new Date().getDate() + (i)));
      const dateMax = new Date(new Date().setDate(new Date().getDate() + (i + 1)));
      let timeMin = this.datePipe.transform(dateMin, 'yyyy-MM-dd');
      let timeMax = this.datePipe.transform(dateMax, 'yyyy-MM-dd');
      timeMin += 'T00:00:00Z';
      timeMax += 'T00:00:00Z';
      const params = {
        timeMin: timeMin,
        timeMax: timeMax
      };

      this.week.push({
        id: i,
        date: new Date(new Date().setDate(new Date().getDate() + (i))),
        params: params,
        isActive: false
      });
    }

    this.setDay(0);
  }


  setDay(id) {
    clearInterval(this.hrInterval);

    this.viewDate = this.week[id].date;
    this.curDayId = id;

    this.week.forEach((it) => {
      it.isActive = false;
    });

    this.week[id].isActive = true;

    this.getCalendarEvents(this.week[id].params);
  }


  beforeHrRender(e): void {
    const check = () => {
      e.body.forEach(hr => {
        hr.segments.forEach((segment) => {
          if (+new Date() >= +new Date(segment.date)) {
            segment.cssClass = 'inactive-hr';
          }
        });
      });
    };

    check();

    this.hrInterval = setInterval(() => {
      check();
    }, 1000 * 60 * 10);
  }


  setStep(val, doc) {
    this.step = val;
    this.curDoc = doc;
  }
}

