import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppointmentAddPopComponent } from './appointment-add.component';

describe('AppointmentAddPopComponent', () => {
  let component: AppointmentAddPopComponent;
  let fixture: ComponentFixture<AppointmentAddPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentAddPopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentAddPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
