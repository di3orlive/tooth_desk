import { Component, OnInit } from '@angular/core';

declare let google: any;

@Component({
  selector: 'app-map-pop',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapPopComponent implements OnInit {
  map: any;

  constructor() { }

  ngOnInit() {
    this.initMap();
  }


  initMap() {
    this.map = new google.maps.Map(document.getElementById('popmap'), {
      center: {lat: 25.25163, lng: 55.301418},
      zoom: 14,
      scrollwheel: false
    });

    const marker = new google.maps.Marker({
      position: {lat: 25.2516302, lng: 55.3014181},
      map: this.map,
      title: 'Dubai Sky Clinic'
    });
  }
}
