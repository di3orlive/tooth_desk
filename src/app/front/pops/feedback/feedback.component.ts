import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent {
  form: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.createForm();
  }


  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      phone: ['', Validators.pattern(/^[\d ()+-]+$/)],
      email: ['', Validators.email],
      feedback: ['', Validators.required]
    });
  }

  submitForm() {
    console.log(this.form.value);
  }
}
