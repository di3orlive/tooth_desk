import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallMePopComponent } from './call-me.component';

describe('CallMePopComponent', () => {
  let component: CallMePopComponent;
  let fixture: ComponentFixture<CallMePopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallMePopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallMePopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
