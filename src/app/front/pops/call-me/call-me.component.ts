import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-call-me',
  templateUrl: './call-me.component.html',
  styleUrls: ['./call-me.component.scss']
})
export class CallMePopComponent {
  form: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<CallMePopComponent>,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      phone: ['', Validators.pattern(/^[\d ()+-]+$/)],
    });
  }

  close() {
    this.dialogRef.close();
  }
}
