import {Component, HostBinding, OnInit} from '@angular/core';
import {SafeSubscribe} from '../../../common/helpers/safe-subscripe/safe-subscripe';
import {ActivatedRoute} from '@angular/router';
import {Animations} from '../../animations/animations';
import {DoctorsService} from '../../services/doctors/doctors.service';

@Component({
  selector: 'app-team-article',
  templateUrl: './team-article.component.html',
  styleUrls: ['./team-article.component.scss'],
  animations: Animations.page
})
export class TeamArticleComponent extends SafeSubscribe implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  public carouselOne = {
    grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
    point: {
      visible: true
    },
    load: 2,
    touch: true,
    loop: true,
    custom: 'banner'
  };
  docId: any;
  doctors: any;
  doctor: any;


  constructor(
    private route: ActivatedRoute,
    private doctorsService: DoctorsService
  ) {
    super();
    this.route.params.safeSubscribe(this, params => {
      this.docId = params['docId'];
    });
  }


  ngOnInit() {
    this.doctorsService.doctors.safeSubscribe(this, res => {
      if (res) {
        this.doctors = res;

        this.doctors.forEach(item => {
          if (item.docId === this.docId) {
            this.doctor = item;
          }
        });
      }
    });
  }
}
