import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';
import {NgxGalleryAnimation} from 'ngx-gallery';

@Component({
  selector: 'app-our-clinic',
  templateUrl: './our-clinic.component.html',
  styleUrls: ['./our-clinic.component.scss'],
  animations: Animations.page
})
export class OurClinicComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;

  galleryOptions = [
    {
      width: '80%',
      height: '500px',
      previewFullscreen: true,
      previewCloseOnClick: true,
      imageSwipe: true,
      thumbnailsSwipe: true,
      thumbnails: false,
      imagePercent: 100,
      thumbnailsPercent: 50,
      imageAnimation: NgxGalleryAnimation.Fade
    }
  ];
  skyClinic = [
    {
      small: 'assets/i/our_clinic/1.jpg',
      medium: 'assets/i/our_clinic/1.jpg',
      big: 'assets/i/our_clinic/1.jpg'
    },
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    },
    {
      small: 'assets/i/our_clinic/1.jpg',
      medium: 'assets/i/our_clinic/1.jpg',
      big: 'assets/i/our_clinic/1.jpg'
    },
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    },
    {
      small: 'assets/i/our_clinic/1.jpg',
      medium: 'assets/i/our_clinic/1.jpg',
      big: 'assets/i/our_clinic/1.jpg'
    },
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    }
  ];
  ourPassion = [
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    },
    {
      small: 'assets/i/our_clinic/1.jpg',
      medium: 'assets/i/our_clinic/1.jpg',
      big: 'assets/i/our_clinic/1.jpg'
    },
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    },
    {
      small: 'assets/i/our_clinic/1.jpg',
      medium: 'assets/i/our_clinic/1.jpg',
      big: 'assets/i/our_clinic/1.jpg'
    },
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    },
    {
      small: 'assets/i/our_clinic/1.jpg',
      medium: 'assets/i/our_clinic/1.jpg',
      big: 'assets/i/our_clinic/1.jpg'
    },
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    },
    {
      small: 'assets/i/our_clinic/1.jpg',
      medium: 'assets/i/our_clinic/1.jpg',
      big: 'assets/i/our_clinic/1.jpg'
    },
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    },
    {
      small: 'assets/i/our_clinic/1.jpg',
      medium: 'assets/i/our_clinic/1.jpg',
      big: 'assets/i/our_clinic/1.jpg'
    },
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    },
    {
      small: 'assets/i/our_clinic/1.jpg',
      medium: 'assets/i/our_clinic/1.jpg',
      big: 'assets/i/our_clinic/1.jpg'
    },
    {
      small: 'assets/i/our_clinic/2.jpg',
      medium: 'assets/i/our_clinic/2.jpg',
      big: 'assets/i/our_clinic/2.jpg'
    }
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
