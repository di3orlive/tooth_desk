import {Component, HostBinding, OnInit} from '@angular/core';
import {SafeSubscribe} from '../../../common/helpers/safe-subscripe/safe-subscripe';
import {Animations} from '../../animations/animations';
import {ServicesService} from '../../services/services/services.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styles: [':host { display: block; }'],
  animations: Animations.page
})
export class ServicesComponent extends SafeSubscribe implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  services: any;

  constructor(
    private servicesService: ServicesService
  ) {
    super();
  }

  ngOnInit() {
    this.servicesService.services.safeSubscribe(this, res => {
      this.services = res;
    });
  }

}
