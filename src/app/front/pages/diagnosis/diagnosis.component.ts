import {Component, HostBinding, OnInit, ViewChild} from '@angular/core';
import {Animations} from '../../animations/animations';
import {AppointmentPopComponent} from '../../pops/appointment/appointment.component';
import {MatDialog} from '@angular/material';


@Component({
  selector: 'app-diagnosis',
  templateUrl: './diagnosis.component.html',
  styleUrls: ['./diagnosis.component.scss'],
  animations: Animations.page
})
export class DiagnosisComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @ViewChild('stepper') stepper;

  symptoms = [
    {
      name: 'symptom 1',
      symptoms: [
        {
          name: 'symptom 2',
          symptoms: [
            {
              name: 'symptoms 3',
              symptoms: [
                {
                  name: 'symptom 4',
                  symptoms: [],
                  diagnosis: 'Periostitis'
                },
              ]
            }
          ]
        },
        {
          name: 'symptom 2.1',
          symptoms: [
            {
              name: 'symptoms 3.1',
              symptoms: [],
              diagnosis: 'Periostitis'
            }
          ]
        }
      ]
    }
  ];

  // symptoms  = [
  //   [
  //     {
  //       symptom: 'Toothache',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Sore and bleeding gum',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Sore tooth extraction hole',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Precipitated seal',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Teeth curves, incorrectly positioned',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Not like the shape of teeth',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Not like the color of teeth',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Bad breath',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Baring the roots of teeth',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Broke away portion of the tooth',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Missing tooth (teeth)',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Appeared tooth mobility',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     },
  //     {
  //       symptom: 'Appeared cheeks edema (flux)',
  //       symptoms: [
  //         {
  //           symptom: 'Toothache after treatment',
  //           symptoms: [
  //             {symptom: 'Sealed up channels'},
  //             {symptom: 'Put seals'},
  //           ]
  //         },
  //         {symptom: 'pain occurs when brushing teeth'},
  //         {symptom: 'pain appeared after a blow to the face'},
  //         {symptom: 'The pain is worse when touched tooth'},
  //         {symptom: 'gnawing pain'},
  //         {symptom: 'responds to changes in temperature'}
  //       ]
  //     }
  //   ]
  // ];



  constructor(
    public dialog: MatDialog
  ) {}



  ngOnInit() {
  }

  addSymptom(item, index) {
    console.log(this.stepper);
    this.symptoms.splice(index + 1, this.symptoms.length);
    this.symptoms[index + 1] = item;
    this.stepper.selectedIndex = index + 2;
  }

  popSymptom() {
    this.symptoms.pop();
  }

  resetSymptoms() {
    this.symptoms.splice(1);
  }

  appointmentPop() {
    this.dialog.open(AppointmentPopComponent);
  }
}
