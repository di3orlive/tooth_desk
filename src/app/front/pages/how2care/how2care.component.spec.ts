import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { How2careComponent } from './how2care.component';

describe('How2careComponent', () => {
  let component: How2careComponent;
  let fixture: ComponentFixture<How2careComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ How2careComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(How2careComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
