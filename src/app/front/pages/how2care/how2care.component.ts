import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-how2care',
  templateUrl: './how2care.component.html',
  styles: [':host { display: block; }'],
  animations: Animations.page
})
export class How2careComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;

  tabs = [
    {
      label: 'Caring for Your Teeth',
      steps: [
        {
          img: 'assets/i/how2care/1/1.jpg',
          label: 'Brush each morning and each evening.',
          text: `When you brush you should do so for at least two minutes. This will give you the time you need to get to all surfaces of your teeth. Make sure you remember the backs of your teeth.
  <ul>
    <li>Teach your children good oral hygiene habits by having them start brushing as soon as they get their first baby teeth. Cavities in baby teeth will be just as uncomfortable as cavities in permanent teeth.
    </li>
    <li>For best results, use a soft bristled brush or an electric toothbrush. Whichever type of toothbrush you use, be sure to replace it every three months. If you are concerned that your toothbrush might be worn out earlier, you can inspect it to see if the bristles are bent and damaged. If so, you may want to replace it.</li>
    <li>Brushing regularly will not only give you healthy, pain-free teeth, but it will also keep your breath fresh. But don’t brush too soon after eating. After you eat your mouth is more acidic and this makes the enamel temporarily softer. Wait at least 30 minutes before brushing.
    </li>
  </ul>`,
        },
        {
          img: 'assets/i/how2care/1/2.jpg',
          label: 'Use a fluoride toothpaste.',
          text: `Fluoride protects the enamel and makes you less likely to get cavities. But it is important that the toothpaste have enough fluoride to be effective. Check to make sure it has at least 1,350 – 1,500 ppm.
  <ul>
    <li>Toothpastes with less than 1,000 ppm of fluoride will be ineffective at protecting your teeth.</li>
    <li>Children can use full strength toothpaste if an adult makes sure they spit it out after brushing.</li>
  </ul>`,
        },
        {
          img: 'assets/i/how2care/1/3.jpg',
          label: 'Floss daily.',
          text: `Flossing cleans the surfaces between your teeth. This is important because you can’t clean there effectively with your toothbrush. As a result, if you don’t floss, food, plaque, and bacteria build up there.
  <ul>
    <li>Use 30 cm of floss or an interdental cleaner. Insert it between your teeth and curve it around one of the teeth. Move it up and down along the side of the tooth, then curve around the other tooth and repeat.
    </li>
    <li>Use a floss pick if you have trouble using the classic method.</li>
    <li>Be gentle when you go below the gum line. If you are new to flossing, your gums may bleed, but after a few days, this will stop.</li>
  </ul>`,
        },
        {
          img: 'assets/i/how2care/1/4.jpg',
          label: 'Use mouthwash.',
          text: `Look for a mouthwash that has fluoride in it. If you use a fluoride mouthwash after brushing and flossing, this will help the fluoride reach the enamel of all surfaces of your teeth. Swish the mouthwash around in your mouth for two minutes to allow it to thoroughly coat all areas of your teeth.
<ul>
  <li>Chlorhexidine is also a protective substance found in some mouthwashes, which fights against bacteria and has a slight remineralizing effect.</li>
  <li>You can also make a natural remedy using a salt solution. Put half teaspoon of salt into a glass of warm water and stir.</li>
  <li>Don’t swallow the mouthwash because it may upset your stomach. If you want to try to kill bacteria further back in your throat, you can gargle briefly before spitting it out.</li>
</ul>`,
        },
        {
          img: 'assets/i/how2care/1/5.jpg',
          label: 'Brush or scrape your tongue.',
          text: `The surface of your tongue is uneven. This means that food particles and bacteria often get trapped there. This then becomes a source of bacteria which gets transferred to your teeth.
<ul>
  <li>You can brush gently or use a special tongue scraper. Some toothbrushes even have a rough, rubbery patch on the back which you can use for this purpose.</li>
  <li>Scrape gently so that you don’t injure your tongue. It shouldn’t hurt. When you finish, rinse your mouth out to get rid of the debris and bacteria.</li>
</ul>`,
        },
        {
          img: 'assets/i/how2care/1/6.jpg',
          label: 'Don’t smoke.',
          text: `Smoking will stain your teeth yellow, give you bad breath, and make you more likely to get gum disease or mouth cancers. If you need help 
<ul>
  <li>Get support from friends, family, or a support group</li>
  <li>Avoid situations where you habitually smoke</li>
  <li>Call a hotline when you are having cravings</li>
  <li>Talk to your doctor or see an addictions counselor</li>
  <li>Try nicotine replacement therapy</li>
  <li>Get residential treatment in a treatment center</li>
</ul>`,
        }
      ]
    },
    {
      label: 'Protecting Your Teeth with a Healthy Diet',
      steps: [
        {
          img: 'assets/i/how2care/2/1.jpg',
          label: 'Limit your sugar consumption',
          text: `Sugar erodes the enamel on your teeth because as it breaks down, it produces acid which wears away your enamel. This makes you more likely to get cavities and tooth decay.
<ul>
<li>Sugary sodas. Limit the amount of juice you drink to one glass per day.</li>
<li>Desserts such as pastries, cakes, ice cream and candies.</li>
<li>Highly sweetened tea or coffee.</li>
</ul>`,
        },
        {
          img: 'assets/i/how2care/2/2.jpg',
          label: 'Eat fewer sticky foods.',
          text: `These foods leave a thin layer of sugar on your teeth which is difficult to remove and increases your risk of tooth decay. Also sticky foods can ruin previous dental work, such as one of your crowns getting pulled off.Try to avoid eating:
<ul>
<li>Gummy candies</li>
<li>Granola bars</li>
<li>Taffy</li>
<li>Dried fruit like raisins</li>
<li>Sugary gum. Sugar-free gum is excellent for stimulating saliva production and helping to remove the last little bits of food stuck between your teeth
</li>
</ul>`,
        },
        {
          img: 'assets/i/how2care/2/3.jpg',
          label: 'Scrape your teeth clean with a crunchy fruit or veggie.',
          text: `This is particularly effective at the end of a meal or as a healthy snack between meals. However, keep in mind that some fruits and vegetables may also stain your teeth, so you'll still need to brush your teeth afterwards. Consider eating more:
<ul>
<li>Apples</li>
<li>Broccoli</li>
<li>Peppers</li>
<li>Carrots</li>
<li>Lettuce</li>
<li>Cucumbers</li>
<li>Celery</li>
<li>Peanuts</li>
<li>Goji berries</li>
<li>Seeds</li>
</ul>`,
        },
        {
          img: 'assets/i/how2care/2/4.jpg',
          label: 'Reduce the amount of alcohol you drink.',
          text: `Alcohol damages the enamel on your teeth and increases your risk of tooth decay. If you need help <a href="/Quit-Drinking-Alcohol" title="Quit Drinking Alcohol">quitting drinking</a> there are numerous resources you can draw upon. You can:<sup id="_ref-17" class="reference" aria-label="Link to Reference 17"><a href="#_note-17">[17]</a></sup><sup id="_ref-18" class="reference" aria-label="Link to Reference 18"><a href="#_note-18">[18]</a></sup>
<ul>
<li>Get support from friends, family, or a support group like Alcoholic Anonymous</li>
<li>Talk to your doctor about taking medications</li>
<li>Get counseling</li>
<li>Try residential treatment</li>
</ul>`,
        }
      ]
    },
    {
      label: 'Getting Professional Care for Your Teeth',
      steps: [
        {
          img: 'assets/i/how2care/3/1.jpg',
          label: 'Go to a dentist if you notice a problem developing.',
          text: `Don’t wait until it’s extremely painful. If you don’t have dental insurance, you may be able to find affordable care by contacting dental schools, searching for free clinics online at the websites of organizations like the American Dental Association or the American Dental Hygienists Association, or contacting your community health center or local health department. Signs that you need to have your teeth checked include:
<ul>
<li>Pain</li>
<li>Permanent teeth that are loose</li>
<li>Red, swollen, or painful gums</li>
<li>Swelling in your jaw</li>
<li>Bad breath or a strange taste in your mouth that doesn’t go away</li>
<li>Sensitivity to the temperature of your food</li>
</ul>`,
        },
        {
          img: 'assets/i/how2care/3/2.jpg',
          label: 'Let the dentist polish your teeth.',
          text: `For best long-term results, you should have your teeth examined and cleaned twice a year. This will involve:
<ul>
<li>Inspecting your teeth for signs of decay</li>
<li>Teaching you how to brush and floss most effectively</li>
<li>Cleaning each surface of each tooth</li>
<li>Scraping away hard plaques that have built up</li>
</ul>`,
        },
        {
          img: 'assets/i/how2care/3/3.jpg',
          label: 'Get protective treatments.',
          text: `These treatments make your teeth less vulnerable to decay. Many people, both children and adults, get fissure sealants and fluoride varnishes.
<ul>
<li>Fissures are the little crevices on the chewing surfaces of your teeth. This treatment involves putting a thin plastic coating on the teeth to prevent cavities from starting in them. This procedure is done on permanent teeth and is good for a decade, but it needs annual check from your dentist.</li>
<li>Fluoride varnish is a strong fluoride solution which strengthens the enamel. It can be applied twice a year on both baby teeth and permanent teeth.</li>
</ul>`,
        }
      ]
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
