import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesArticleComponent } from './services-article.component';

describe('ServicesArticleComponent', () => {
  let component: ServicesArticleComponent;
  let fixture: ComponentFixture<ServicesArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
