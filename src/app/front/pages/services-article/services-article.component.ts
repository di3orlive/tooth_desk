import {Component, HostBinding, OnInit} from '@angular/core';
import {SafeSubscribe} from '../../../common/helpers/safe-subscripe/safe-subscripe';
import {Animations} from '../../animations/animations';
import {ActivatedRoute} from '@angular/router';
import {ServicesService} from '../../services/services/services.service';

@Component({
  selector: 'app-services-article',
  templateUrl: './services-article.component.html',
  styleUrls: ['./services-article.component.scss'],
  animations: Animations.page
})
export class ServicesArticleComponent extends SafeSubscribe implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  serviceId: any;
  services: any;
  service: any;

  constructor(
    private servicesService: ServicesService,
    private route: ActivatedRoute
  ) {
    super();

    this.route.params.safeSubscribe(this, params => {
      this.serviceId = params['serviceId'];
    });
  }

  ngOnInit() {
    this.servicesService.services.safeSubscribe(this, res => {
      if (res) {
        this.services = res;

        this.services.forEach(item => {
          if (item.id === this.serviceId) {
            this.service = item;
          }
        });
      }
    });
  }
}
