import {Component, HostBinding, OnInit} from '@angular/core';
import {NgxGalleryAnimation} from 'ngx-gallery';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-awards',
  templateUrl: './awards.component.html',
  styleUrls: ['./awards.component.scss'],
  animations: Animations.page
})
export class AwardsComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;

  galleryOptions = [
    {
      width: '80%',
      height: '500px',
      previewFullscreen: true,
      previewCloseOnClick: true,
      imageSwipe: true,
      thumbnailsSwipe: true,
      thumbnails: false,
      imagePercent: 100,
      thumbnailsPercent: 50,
      imageAnimation: NgxGalleryAnimation.Fade
    }
  ];
  awards = [
    {
      small: 'assets/i/awards/1.jpg',
      medium: 'assets/i/awards/1.jpg',
      big: 'assets/i/awards/1.jpg'
    },
    {
      small: 'assets/i/awards/2.jpg',
      medium: 'assets/i/awards/2.jpg',
      big: 'assets/i/awards/2.jpg'
    },
    {
      small: 'assets/i/awards/3.jpg',
      medium: 'assets/i/awards/3.jpg',
      big: 'assets/i/awards/3.jpg'
    },
    {
      small: 'assets/i/awards/4.jpg',
      medium: 'assets/i/awards/4.jpg',
      big: 'assets/i/awards/4.jpg'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
