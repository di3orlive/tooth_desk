import {Component, HostBinding, OnInit} from '@angular/core';
import {SafeSubscribe} from '../../../common/helpers/safe-subscripe/safe-subscripe';
import {Animations} from '../../animations/animations';
import {MatDialog} from '@angular/material';
import {AppointmentPopComponent} from '../../pops/appointment/appointment.component';
import {ServicesService} from '../../services/services/services.service';

declare let google: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: Animations.page
})
export class HomeComponent extends SafeSubscribe implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  services: any;


  constructor(
    public dialog: MatDialog,
    private servicesService: ServicesService
  ) {
    super();
  }


  ngOnInit() {
    this.servicesService.services.safeSubscribe(this, res => {
      this.services = res;
    });
  }


  initMap() {
    return new google.maps.StreetViewPanorama(
      document.getElementById('map'), {
        position: {lat: 49.4495054, lng: 32.0492811},
        pov: {heading: -20, pitch: 10},
        zoom: 0
      });
  }


  appointmentPop() {
    this.dialog.open(AppointmentPopComponent);
  }
}
