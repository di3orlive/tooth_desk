import {Component, HostBinding, OnInit} from '@angular/core';
import {Animations} from '../../animations/animations';

@Component({
  selector: 'app-educational',
  templateUrl: './educational.component.html',
  styleUrls: ['./educational.component.scss'],
  animations: Animations.page
})
export class EducationalComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  videos = [
    {
      url: 'https://www.youtube.com/embed/aJZSOnFaOF8',
      loading: true
    },
    {
      url: 'https://www.youtube.com/embed/78r3d4qa9A0',
      loading: true
    },
    {
      url: 'https://www.youtube.com/embed/nIgivHNdoBM',
      loading: true
    },
    {
      url: 'https://www.youtube.com/embed/78r3d4qa9A0',
      loading: true
    },
    {
      url: 'https://www.youtube.com/embed/aJZSOnFaOF8',
      loading: true
    },
    {
      url: 'https://www.youtube.com/embed/nIgivHNdoBM',
      loading: true
    }
  ];

  constructor(
  ) {
  }

  ngOnInit() {
  }

  onVideoLoad(item) {
    if (item) {
      item.loading = false;
    }
  }
}
