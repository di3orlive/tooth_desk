import {Component, HostBinding, OnInit} from '@angular/core';
import {SafeSubscribe} from '../../../common/helpers/safe-subscripe/safe-subscripe';
import {Animations} from '../../animations/animations';
import {DoctorsService} from '../../services/doctors/doctors.service';

@Component({
  selector: 'app-team-page',
  templateUrl: './team.component.html',
  styles: [':host { display: block; }'],
  animations: Animations.page
})
export class TeamPageComponent extends SafeSubscribe implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  doctors: any;

  constructor(
    private doctorsService: DoctorsService
  ) {
    super();
  }

  ngOnInit() {
    this.doctorsService.doctors.safeSubscribe(this, res => {
      this.doctors = res;
    });
  }
}
