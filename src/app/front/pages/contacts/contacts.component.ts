import {Component, HostBinding, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Animations} from '../../animations/animations';
import {AppointmentPopComponent} from '../../pops/appointment/appointment.component';

declare let google: any;

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styles: [':host { display: block; }'],
  animations: Animations.page
})
export class ContactsComponent implements OnInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  map: any;

  constructor(
    public dialog: MatDialog
  ) {}

  ngOnInit() {
  }


  appointmentPop() {
    this.dialog.open(AppointmentPopComponent);
  }


  initMap() {
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 25.197197, lng: 55.2743764},
      zoom: 14,
      scrollwheel: false
    });

    const marker = new google.maps.Marker({
      position: {lat: 25.197197, lng: 55.2743764},
      map: this.map,
      title: 'Dubai Sky Clinic'
    });

    return this.map;
  }

}
