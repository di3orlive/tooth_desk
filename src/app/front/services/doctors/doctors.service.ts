import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class DoctorsService {
  _doctors = new BehaviorSubject<any>(this.defaultDoctors());

  constructor() { }

  get doctors() {
    return this._doctors.asObservable();
  }

  set doctors(a) {
    this._doctors.next(a);
  }

  private defaultDoctors() {
    return [
      {
        docId: 'doc1',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Cosmetic and General Dentist DDS (Germany)',
        credo: 'Aesthetics Is My Way',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      },
      {
        docId: 'doc2',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Orthodontist BDS, DUO (France)',
        credo: 'Perfect Is More Than Just Straight',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      },
      {
        docId: 'doc3',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Oral Surgeon and Implantologist BDS, DES, DESS (Lebanon)',
        credo: 'Simply The Best',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: [
          './assets/i/doctor/abi/cases/1.jpg',
          './assets/i/doctor/abi/cases/2.jpg',
          './assets/i/doctor/abi/cases/3.jpg',
          './assets/i/doctor/abi/cases/4.jpg',
          './assets/i/doctor/abi/cases/5.jpg',
          './assets/i/doctor/abi/cases/6.jpg',
          './assets/i/doctor/abi/cases/7.jpg'
        ]
      },
      {
        docId: 'doc4',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Cosmetic and General Dentist DDS (Serbia)',
        credo: 'Your Smile Is My Mission',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      },
      {
        docId: 'doc5',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Cosmetic, General and Endodontic Dentist',
        credo: 'Boosting your confidence one step at a time',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      },
      {
        docId: 'doc6',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Laser Specialist and General Dentist B.D.S., MSc., EMDOLA (Germany)',
        credo: 'Greet The World With A Smile',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: [
          './assets/i/doctor/yasmin/cases/1.jpg',
          './assets/i/doctor/yasmin/cases/2.jpg'
        ]
      },
      {
        docId: 'doc7',
        img: 'assets/i/doctor/doc.png',
        shortName: 'Dr. Max',
        fullName: 'Dr. Maximilian',
        type: 'Dental Hygienist (Germany)',
        credo: 'It Is Simply Lovely To Have, Show And See Beautiful Teeth',
        text: `<h1 class="docname-title">Dr. Maximilian</h1>
<div class="doctitle">Cosmetic and General Dentist</div>
<h3>Experience</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi, distinctio dolore earum, est laboriosam nam natus nisi, pariatur possimus similique temporibus? A adipisci consectetur corporis cumque doloremque eligendi esse ex exercitationem expedita facere hic illo impedit iusto maxime minima mollitia necessitatibus odit quam recusandae reprehenderit repudiandae, sint soluta voluptate.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis facilis nostrum odio quasi repudiandae sapiente. Nobis praesentium quibusdam voluptatibus.</p>
<h3>Education</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur at, aut beatae cum cumque cupiditate dolor doloremque dolorum id ipsum quae recusandae, repudiandae vero? Dignissimos explicabo fuga officiis quaerat. Accusamus aliquid asperiores, consequuntur cumque delectus doloribus exercitationem illo incidunt, numquam placeat, quibusdam quisquam sequi vero.</p>
<h3>Hobbies and Interests</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consectetur id nihil nobis officiis perferendis porro, quaerat rem tenetur unde.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aspernatur dignissimos minima necessitatibus nostrum recusandae.</p>
<h3>Personal</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, doloribus!</p>
<p class="emaildctr"><a href="mailto:example@example.com">example@example.com</a></p>`,
        gallery: [
          './assets/i/services/teeth_whitening.jpeg'
        ],
        cases: []
      }
    ];
  }
}

