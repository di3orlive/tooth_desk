import { Injectable } from '@angular/core';
import {MatSidenav} from '@angular/material';

@Injectable()
export class SideNavService {
  sideNav: MatSidenav;

  public setSideNav(sideNav: MatSidenav) {
    this.sideNav = sideNav;
  }

  public toggle(isOpen?: boolean): Promise<any> {
    return this.sideNav.toggle(isOpen);
  }
}
