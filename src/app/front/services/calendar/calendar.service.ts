import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class CalendarService {
  api = 'https://di3calendar.herokuapp.com';

  constructor(private http: Http) {}


  checkForErr(err) {
    return Observable.throw(err).catch(error => Observable.of(error));
  }


  getDayEvents(params, CALENDAR_ID) {
    const body = {
      'singleEvents' : true,
      'orderBy' : 'startTime',
      'timeMin':  params.timeMin,
      'timeMax':  params.timeMax
    };


    return this.http.get(`${this.api}/list`, {params: {calendar_id: CALENDAR_ID, body: body}})
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }


  insertEvent(params, CALENDAR_ID) {
    const body = {
      calendar_id: CALENDAR_ID,
      body: params
    };


    return this.http.post(`${this.api}/event`, body)
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }
}













